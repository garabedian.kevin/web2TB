package controller;

import java.util.LinkedHashSet;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import util.Diplome;
import util.Etudiant;
import util.Filiaire;
 import static util.SQLUtil.*;

@Controller
public class MainController {
	
	@RequestMapping("/")
	public ModelAndView getindex(){
		LinkedHashSet<Etudiant> result = getEtudiants();
		return result!=null ? new ModelAndView("index","list",result) : 
			new ModelAndView("error","msg","Erreur lors de la connection à la base de données mysql!");
	}
	
	@RequestMapping("/profil")
	public ModelAndView getprofil(){
		return new ModelAndView("profil");
	}
	
	@RequestMapping("/login")
	public ModelAndView getlogin(){
		return new ModelAndView("login");
	}
	
	@RequestMapping("/creatediplome")
	public ModelAndView getcdiplome(){
		return new ModelAndView("creatediplome");
	}
	
	@RequestMapping("/createprofil")
	public ModelAndView getcprofil(){
		return new ModelAndView("createprofil");
	}
	
	@RequestMapping(value="/resp", method=RequestMethod.GET)
	public @ResponseBody 
	String getajaxresp(){
		String result = "Ajouté";
		return result;
	}
	
}
