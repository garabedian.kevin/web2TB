<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<body>
		<header>
			<div class="col-md-12">
				<%@include file="navbar.jsp" %>
			</div>
		</header>
		<div class="row">
		<div class="col-md-1"></div>
			<div class="col-md-10 well">
				<h1 style="text-align: center">Login</h1>
				<form class="form-horizontal" method="post" action="{site_url('cAccount/login')}" enctype="multipart/form-data">
					<div class="form-group">
						
						<label for="Email" class="col-xs-12 col-sm-6 col-md-2 ">Email</label>
						<div class="col-sm-10">
							<input type="email" class="form-control" id="Email" name="email" placeholder="Email">
						</div>
					</div>
					<div class="form-group">
						
						<label for="Password" class="col-xs-12 col-sm-6 col-md-2 ">Password</label>
						<div class="col-sm-10">
							<input type="password" class="form-control" id="Password" name="pass" placeholder="Password">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" class="btn btn-default" name="action" value="login">Sign in</button>
						</div>
					</div>
				</form>
			</div>
		
			<div class="col-md-1">
			</div>
		</div>
	</body>
</html>