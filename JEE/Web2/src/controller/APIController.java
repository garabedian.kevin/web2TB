package controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.AbstractMap.SimpleEntry;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.stream.JsonParsingException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import util.Diplome;
import util.Etudiant;
import util.Filiaire;

import static util.SQLUtil.*;

@RestController
@RequestMapping("/api")
public class APIController {
	
	@RequestMapping(value="")
    public ModelAndView redirectApi() {
		return new ModelAndView("redirect:api/");
	}
	
	@RequestMapping(value="/")
    public ModelAndView api() {
		return new ModelAndView("api");
	}
	
	@RequestMapping(value="/etudiants",method=RequestMethod.GET)
    public LinkedHashSet<Etudiant> apiEtudiantsGET(HttpServletRequest req) {
		try {
			//Connection au serveur mysql
			Statement stmt = SQLconnect();
			
			String id,prenom,nom,filiaire,ville,tel,email,annee;
			//String age;

			req.setCharacterEncoding("UTF-8");
			id=req.getParameter("id");
			prenom=req.getParameter("prenom");
			nom=req.getParameter("nom");
			filiaire=req.getParameter("filiaire");
			ville=req.getParameter("ville");
			tel=req.getParameter("tel");
			email=req.getParameter("email");
			annee=req.getParameter("annee");
			
			//Création de toutes les paires : nomduchamp , valeur 
			Map.Entry<String, String> pair1=new SimpleEntry<>("ID",id);
			Map.Entry<String, String> pair2=new SimpleEntry<>("Prenom",prenom);
			Map.Entry<String, String> pair3=new SimpleEntry<>("Nom",nom);
			Map.Entry<String, String> pair4=new SimpleEntry<>("Filiaire",filiaire);
			Map.Entry<String, String> pair5=new SimpleEntry<>("Ville",ville);
			Map.Entry<String, String> pair6=new SimpleEntry<>("Telephone",tel);
			Map.Entry<String, String> pair7=new SimpleEntry<>("Email",email);
			Map.Entry<String, String> pair8=new SimpleEntry<>("Naissance",annee);
			
			//Ajout des paires à un Set
			LinkedHashSet<Map.Entry<String,String>> pairs = new LinkedHashSet<>();
			pairs.add(pair1);
			pairs.add(pair2);
			pairs.add(pair3);
			pairs.add(pair4);
			pairs.add(pair5);
			pairs.add(pair6);
			pairs.add(pair7);
			pairs.add(pair8);
			//age=req.getParameter("age");
			
			//Query par défaut
			String query="SELECT * "
					+ "FROM etudiants";
			
			query=SQLparamTests(pairs,query);
				
			//Requête pour récuperer les étudiants
			
			ResultSet queryResult = stmt.executeQuery(query);
			LinkedHashSet<Etudiant> result=queryResultToSetEtudiant(queryResult);
			
			return result;
		} catch (SQLException | UnsupportedEncodingException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {			
			e.printStackTrace();
			return null;
		}
	}
		
	@RequestMapping(value="/etudiants/etu{etuID}",method=RequestMethod.GET)
	public Etudiant apiEtudiantsGET(@PathVariable(value="etuID") String id) {
		try {
			//Connection au serveur mysql
			Statement stmt = SQLconnect();
				
			//Query par défaut
			String query="SELECT * "
					+ "FROM etudiants"
					+ " WHERE ID=\""+id+"\"";
					
			//Requête pour récuperer les étudiants
			
			ResultSet queryResult = stmt.executeQuery(query);
			LinkedHashSet<Etudiant> result=queryResultToSetEtudiant(queryResult);
			return result.iterator().next();
		} catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {			
			e.printStackTrace();
			return null;
		}
	}	
		
	@RequestMapping(value="/etudiants/etu{etuID}",method=RequestMethod.DELETE)
    public @ResponseBody int apiEtudiantsDELETE(@PathVariable(value="etuID") String id) {
		try {
			//Connection au serveur mysql
			Statement stmt = SQLconnect();
			
			//Query par défaut
			String query="DELETE "
					+ "FROM etudiants"
					+ " WHERE ID=\""+id+"\"";
				
			//Requête pour récuperer les étudiants
			
			int rowsAffected=stmt.executeUpdate(query);
			
            return rowsAffected;
			
		} catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {			
			e.printStackTrace();
			return 0;
		}
	}
	
	@RequestMapping(value="/etudiants",method=RequestMethod.POST)
    public @ResponseBody
    int apiEtudiantsPOST(HttpServletRequest req) {
		try {
			//Connection au serveur mysql
			Statement stmt = SQLconnect();
			
			String prenom,nom,filiaire,ville,tel,email,annee,pass;
			//String age;
			
			req.setCharacterEncoding("UTF-8");
			BufferedReader B = req.getReader();

			JsonReader jsonRead=Json.createReader(B);
			try{
				JsonObject json=jsonRead.readObject();
				
				prenom=json.getString("prenom");
				nom=json.getString("nom");
				filiaire=json.getString("filiaire");
				ville=json.getString("ville");
				tel=json.getString("tel");
				email=json.getString("email");
				annee=json.getString("annee");
				pass=json.getString("pass");
				
				//Création de toutes les paires : nomduchamp , valeur 
				Map.Entry<String, String> pair1=new SimpleEntry<>("Prenom",prenom);
				Map.Entry<String, String> pair2=new SimpleEntry<>("Nom",nom);
				Map.Entry<String, String> pair3=new SimpleEntry<>("Filiaire",filiaire);
				Map.Entry<String, String> pair4=new SimpleEntry<>("Ville",ville);
				Map.Entry<String, String> pair5=new SimpleEntry<>("Telephone",tel);
				Map.Entry<String, String> pair6=new SimpleEntry<>("Email",email);
				Map.Entry<String, String> pair7=new SimpleEntry<>("Naissance",annee);
				Map.Entry<String, String> pair8=new SimpleEntry<>("Pass",pass);
				
				//Ajout des paires à un Set
				LinkedHashSet<Map.Entry<String,String>> pairs = new LinkedHashSet<>();
				pairs.add(pair1);
				pairs.add(pair2);
				pairs.add(pair3);
				pairs.add(pair4);
				pairs.add(pair5);
				pairs.add(pair6);
				pairs.add(pair7);
				pairs.add(pair8);
				//age=req.getParameter("age");
				
				//Query par défaut
				String query="Insert into etudiants (Prenom,Nom,Filiaire,Ville,Telephone,Email,Naissance,Pass) values (";
				
				for(Map.Entry<String,String> pair : pairs){	
					if(!pair.getKey().equals("Pass"))
						query+="'"+pair.getValue()+"', ";
					else
						query+="'"+pair.getValue()+"');";
				}
				
				
					
				//Requête pour récuperer les étudiants
				int rowsAffected = stmt.executeUpdate(query);
				
				return rowsAffected;
				
			}catch(JsonParsingException jpe){
				jpe.printStackTrace();
				return -1;		
			}
			
		} catch (SQLException | IOException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {			
			e.printStackTrace();
			return -1;
		}
    }	
	
	@RequestMapping("/filiaires")
    public LinkedHashSet<Filiaire> apiFiliairesGET(HttpServletRequest req) {
		try {
			//Connection au serveur mysql
			Statement stmt = SQLconnect();
			
			String id,nom;
			//String age;

			req.setCharacterEncoding("UTF-8");
			id=req.getParameter("id");
			nom=req.getParameter("nom");
			
			
			//Création de toutes les paires : nomduchamp , valeur 
			Map.Entry<String, String> pair1=new SimpleEntry<>("id",id);
			Map.Entry<String, String> pair2=new SimpleEntry<>("nom",nom);
			
			//Ajout des paires à un Set
			LinkedHashSet<Map.Entry<String,String>> pairs = new LinkedHashSet<>();
			pairs.add(pair1);
			pairs.add(pair2);
			
			//Query par défaut
			String query="SELECT * "
					+ "FROM filiaire";
			
			query=SQLparamTests(pairs,query);
				
			//Requête pour récuperer les étudiants
			
			ResultSet queryResult = stmt.executeQuery(query);
			LinkedHashSet<Filiaire> result=queryResultToSetFiliaire(queryResult);
			
			return result;
		} catch (SQLException | UnsupportedEncodingException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {			
			e.printStackTrace();
			return null;
		}
    }
	
	@RequestMapping(value="/filiaires/fil{filID}",method=RequestMethod.GET)
    public Filiaire apiFiliairesGET(@PathVariable(value="filID") String id) {
		try {
			//Connection au serveur mysql
			Statement stmt = SQLconnect();
			
			//Query par défaut
			String query="SELECT * "
					+ "FROM filiaire"
					+ " WHERE ID=\""+id+"\"";
				
			//Requête pour récuperer les étudiants
			
			ResultSet queryResult = stmt.executeQuery(query);
			LinkedHashSet<Filiaire> result=queryResultToSetFiliaire(queryResult);
			return result.iterator().next();
		} catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {			
			e.printStackTrace();
			return null;
		}
	}
	
	@RequestMapping(value="/filiaires/fil{filID}",method=RequestMethod.DELETE)
    public @ResponseBody int apiFiliairesDELETE(@PathVariable(value="filID") String id) {
		try {
			//Connection au serveur mysql
			Statement stmt = SQLconnect();
			
			//Query par défaut
			String query="DELETE "
					+ "FROM filiaires"
					+ " WHERE ID=\""+id+"\"";
				
			//Requête pour récuperer les étudiants
			
			int rowsAffected=stmt.executeUpdate(query);

			return rowsAffected;

		} catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	@RequestMapping("/diplomes")
    public LinkedHashSet<Diplome> apiDiplomesGET(HttpServletRequest req) {
		try {
			//Connection au serveur mysql
			Statement stmt = SQLconnect();
			
			String id,nom,annee,lieu;
			//String age;

			req.setCharacterEncoding("UTF-8");
			id=req.getParameter("id");
			nom=req.getParameter("nom");
			annee=req.getParameter("annee");
			lieu=req.getParameter("lieu");
			
			//Création de toutes les paires : nomduchamp , valeur 
			Map.Entry<String, String> pair1=new SimpleEntry<>("ID",id);
			Map.Entry<String, String> pair2=new SimpleEntry<>("Nom",nom);
			Map.Entry<String, String> pair3=new SimpleEntry<>("Année",annee);
			Map.Entry<String, String> pair4=new SimpleEntry<>("Lieu",lieu);
			
			//Ajout des paires à un Set
			LinkedHashSet<Map.Entry<String,String>> pairs = new LinkedHashSet<>();
			pairs.add(pair1);
			pairs.add(pair2);
			pairs.add(pair3);
			pairs.add(pair4);
			
			//Query par défaut
			String query="SELECT * "
					+ "FROM diplomes";
			
			query=SQLparamTests(pairs,query);
				
			//Requête pour récuperer les étudiants
			
			ResultSet queryResult = stmt.executeQuery(query);
			LinkedHashSet<Diplome> result=queryResultToSetDiplome(queryResult);
			
			
			return result;
		} catch (SQLException | UnsupportedEncodingException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {			
			e.printStackTrace();
			return null;
		}
    }
	
	@RequestMapping(value="/diplomes/dipl{diplID}",method=RequestMethod.GET)
    public Diplome apiDiplomesGET(@PathVariable(value="diplID") String id) {
		try {
			//Connection au serveur mysql
			Statement stmt = SQLconnect();
			
			//Query par défaut
			String query="SELECT * "
					+ "FROM diplomes"
					+ " WHERE ID=\""+id+"\"";
				
			//Requête pour récuperer les étudiants
			
			ResultSet queryResult = stmt.executeQuery(query);
			LinkedHashSet<Diplome> result=queryResultToSetDiplome(queryResult);
			if(result.iterator().hasNext())
				return result.iterator().next();
			return null;
		} catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {			
			e.printStackTrace();
			return null;
		}
	}	
	
	@RequestMapping(value="/diplomes/dipl{diplID}",method=RequestMethod.DELETE)
    public @ResponseBody int apiDiplomesDELETE(@PathVariable(value="diplID") String id) {
		try {
			
			//Connection au serveur mysql
			Statement stmt = SQLconnect();
			
			//Query par défaut
			String query="DELETE "
					+ "FROM diplomes"
					+ " WHERE ID=\""+id+"\"";
				
			//Requête pour récuperer les étudiants
			
			int rowsAffected=stmt.executeUpdate(query);

			return rowsAffected;
			
		} catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
			return 0;
		}
	}

}
