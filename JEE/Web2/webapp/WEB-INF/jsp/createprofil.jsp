<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<body onload="blur()">
	<script type="text/javascript" src=<c:url value="/resources/js/jsonform.js"/>></script>
		<header>
			<div class="col-md-12">
				<%@include file="navbar.jsp" %>
			</div>
		</header>
		<div class="row">
		<div class="col-md-1">
		</div>
		<div class="col-md-10 well">
			<h1 style="text-align: center">Créer un profil</h1>
			<form id="jsonForm" class="form-horizontal">
				<div class="form-group">					
					<label for="prenom" class="col-xs-12 col-sm-6 col-md-2 ">Prenom</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="prenom" name="prenom" placeholder="Prenom">
					</div>
				</div>
				<div class="form-group">					
					<label for="nom" class="col-xs-12 col-sm-6 col-md-2 ">Nom</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="nom" name="nom" placeholder="Nom">
					</div>
				</div>
				<div class="form-group">					
					<label for="filaire" class="col-xs-12 col-sm-6 col-md-2 ">Filiaire</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="filaire" name="filiaire" placeholder="Filiaire">
					</div>
				</div>
				<div class="form-group">					
					<label for="Ville" class="col-xs-12 col-sm-6 col-md-2 ">Ville</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="Ville" name="ville" placeholder="Ville">
					</div>
				</div>
				<div class="form-group">					
					<label for="Telephone" class="col-xs-12 col-sm-6 col-md-2 ">Telephone</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="Telephone" name="tel" placeholder="Telephone">
					</div>
				</div>
				<div class="form-group">					
					<label for="dob" class="col-xs-12 col-sm-6 col-md-2 ">Date de naissance</label>
					<div class="col-sm-10">
						<input type="date" class="form-control" id="dob" name="annee" placeholder="aaaa-mm-jj">
					</div>
				</div>
				<div class="form-group">					
					<label for="Email" class="col-xs-12 col-sm-6 col-md-2 ">Email</label>
					<div class="col-sm-10">
						<input type="email" class="form-control" id="Email" name="email" placeholder="Email">
					</div>
				</div>
				<div class="form-group">				
					<label for="Password" class="col-xs-12 col-sm-6 col-md-2 ">Mot de passe</label>
					<div class="col-sm-10">
						<input type="password" class="form-control" id="Password" name="pass" placeholder="Password">
					</div>
				</div>
				<div class="form-group">				
					<label for="Passwordc" class="col-xs-12 col-sm-6 col-md-2 ">Confirmation du mot de passe</label>
					<div class="col-sm-10">
						<input type="password" class="form-control" id="Passwordc" name="cPass" placeholder="Password">
					</div>
				</div>
				<div class="form-group">					
					<label for="imgProfil" class="col-xs-12 col-sm-6 col-md-2 ">Image de profil</label>
					<input type="file" id="imgProfil" name="imgProfil" class="col-xs-12 col-sm-6 col-md-2 ">
					<p class="help-block">Veuillez choisir une image de profil</p>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<input type="button" class="btn btn-default" onclick="jsonpost()" value="Valider"/>
					</div>
				</div>
				<div class="col-sm-offset-2 col-sm-10">
					<span id="return"></span>
					</div>
			</form>
		</div>
		<div class="col-md-1">
		</div>
		</div>
	</body>
</html>