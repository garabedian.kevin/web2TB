package util;

public class Diplome {
	private int ID,annee;
	private String nom,lieu;

	public Diplome(){
		ID=annee=0;
		nom=lieu="N/A";
	}
	
	public Diplome(int ID,int annee, String nom, String lieu){
		this.ID=ID;
		this.annee=annee;
		this.nom=nom;
		this.lieu=lieu;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public int getAnnee() {
		return annee;
	}

	public void setAnnee(int annee) {
		this.annee = annee;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getLieu() {
		return lieu;
	}

	public void setLieu(String lieu) {
		this.lieu = lieu;
	}
	
}
