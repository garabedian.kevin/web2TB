package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.LinkedHashSet;
import java.util.Map;

public final class SQLUtil {
	/**
	 * Le nom d'utilisateur du serveur mysql
	 */
	private static final String mysql_user="root";
	/**
	 * Le mot de passe du serveur mysql
	 */
	private static final String mysql_pass="toor";
	
	/**
	 * Le constructeur par défaut
	 */
	private SQLUtil(){}
	
	/**
	 * Crée une connection à la base de données mysql
	 * @return Le Statement associé à la connection
	 * @throws SQLException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 */
	public static Statement SQLconnect() throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException{
		//Register du driver
		Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
		Connection con = DriverManager.getConnection(
				"jdbc:mysql://localhost/Web2?useSSL=false&serverTimezone=Europe/Paris",getMysqlUser(),getMysqlPass());
		return con.createStatement();
	}

	/**
	 * @brief Effectue l'ajout des tests présents dans pairs
	 * @param pairs
	 * @param query
	 * @return La requête avec les tests
	 */
	public static String SQLparamTests(LinkedHashSet<Map.Entry<String,String>> pairs,String query){
		for(Map.Entry<String,String> pair : pairs){
			if(pair.getValue()!=null){
				query=linkSQLCondition(query);
				query+=" "+pair.getKey()+"=\""+pair.getValue()+"\"";
			}
		}
		return query;
	}
	
	/**
	 * Récupère tous les étudiants de la base de données
	 * @return L'ensemble des étudiants
	 */
	public static LinkedHashSet<Etudiant> getEtudiants(){
		try {
			//Connection au serveur mysql
			Statement stmt = SQLconnect();
			
			//Requête pour récuperer les étudiants
			ResultSet queryResult = stmt.executeQuery("SELECT * FROM etudiants");			
			
			//Liste construite à partir du résultat de la requête
			return queryResultToSetEtudiant(queryResult);
		} catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {			
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * @brief Transforme un ResultSet en ensemble d'étudiants 
	 * @param queryResult
	 * @return L'ensemble d'étudiants
	 * @throws NumberFormatException
	 * @throws SQLException
	 */
	public static LinkedHashSet<Etudiant> queryResultToSetEtudiant(ResultSet queryResult) throws NumberFormatException, SQLException{
		LinkedHashSet<Etudiant> result = new LinkedHashSet<>();
		while(queryResult.next()){
			Etudiant student = new Etudiant();
			student.setID(queryResult.getInt("ID"));
			student.setPrenom(queryResult.getString("Prenom"));
			student.setNom(queryResult.getString("Nom"));
			student.setFiliaire(queryResult.getString("Filiaire"));
			student.setVille(queryResult.getString("Ville"));
			student.setTel(0+queryResult.getString("Telephone"));
			student.setEmail(queryResult.getString("Email"));
			String D=queryResult.getString("Naissance");				
			student.setAge(getAge(D));
			student.setNaissance(D);
			result.add(student);
		}
		return result;
	}
	
	/**
	 * Transforme un ResultSet en ensemble de filiaires
	 * @param queryResult
	 * @return L'ensemble de filiaires
	 * @throws NumberFormatException
	 * @throws SQLException
	 */
	public static LinkedHashSet<Filiaire> queryResultToSetFiliaire(ResultSet queryResult) throws NumberFormatException, SQLException{
		LinkedHashSet<Filiaire> result = new LinkedHashSet<>();	
		while(queryResult.next()){
			Filiaire path = new Filiaire();
			path.setId(queryResult.getInt("id"));
			path.setNom(queryResult.getString("nom"));						
			result.add(path);
		}
		return result;
	}
	
	/**
	 * Transforme un ResultSet en ensemble de diplômes
	 * @param queryResult
	 * @return L'ensemble de diplômes
	 * @throws NumberFormatException
	 * @throws SQLException
	 */
	public static LinkedHashSet<Diplome> queryResultToSetDiplome(ResultSet queryResult) throws NumberFormatException, SQLException{
		LinkedHashSet<Diplome> result = new LinkedHashSet<>();
		while(queryResult.next()){
			Diplome diploma = new Diplome();
			diploma.setID(queryResult.getInt("ID"));
			diploma.setNom(queryResult.getString("Nom"));						
			diploma.setAnnee(queryResult.getInt("Année"));
			diploma.setLieu(queryResult.getString("Lieu"));
			result.add(diploma);
		}
		return result;
	}
	
	/**
	 * Calcule la différence entre la date courante et une date donnée
	 * @param date La date antérieure à comparer
	 * @return La différence entre la date courante et la date donnée
	 */
	public static int getAge(String date){
		int result=0;
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		int currentMonth = Calendar.getInstance().get(Calendar.MONTH);
		int currentDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
		String Smonth;
		Smonth=date.replaceAll("^.*?-", "");
		Smonth=Smonth.replaceAll("-.*$", "");
		int year = Integer.parseInt(date.replaceAll("-.*", ""));
		int month = Integer.parseInt(Smonth);
		int day = Integer.parseInt(date.replaceAll("^.*-.*-", ""));
		
		if(currentYear<year || (currentYear==year && currentMonth<month) || (currentYear==year && currentMonth==month && currentDay<day)){
			return -1;
		}
		
		result=currentYear-year;
		if(currentMonth<month || (currentMonth==month && currentDay<day)){
			result--;
		}
		return result;
		
	}
	
	/**
	 * Permet d'ajouter des conditions à une requete SQL
	 * @param query La requête à laquelle ajouter les conditions
	 * @return La requête finale
	 */
		public static String linkSQLCondition(String query){
			if(!query.contains("WHERE"))
				query+=" WHERE";
			else
				query+=" and";
			return query;
		}

		public static String getMysqlUser() {
			return mysql_user;
		}

		public static String getMysqlPass() {
			return mysql_pass;
		}

}
