package util;

public class Etudiant {
	private String Prenom,Nom,Filiaire,Ville,Tel,Email,Naissance;
	private int ID,Age;
	
	public Etudiant(){
		Prenom=Nom=Filiaire=Ville=Tel=Email="N/A";
		Age=0;
	}
	
	public Etudiant(int ID,String Prenom,String Nom,String Filiaire,String Ville, String Tel, String Email, int Age,String Naissance){
		this.ID=ID;
		this.Prenom=Prenom;
		this.Nom=Nom;
		this.Filiaire=Filiaire;
		this.Ville=Ville;
		this.Tel=Tel;
		this.Email=Email;
		this.Age=Age;
		this.Naissance=Naissance;
	}

	public String getPrenom() {
		return Prenom;
	}

	public void setPrenom(String prenom) {
		Prenom = prenom;
	}

	public String getNom() {
		return Nom;
	}

	public void setNom(String nom) {
		Nom = nom;
	}

	public String getFiliaire() {
		return Filiaire;
	}

	public void setFiliaire(String filiaire) {
		Filiaire = filiaire;
	}

	public int getAge() {
		return Age;
	}

	public void setAge(int Age) {
		this.Age = Age;
	}

	public String getVille() {
		return Ville;
	}

	public void setVille(String ville) {
		Ville = ville;
	}

	public String getTel() {
		return Tel;
	}

	public void setTel(String tel) {
		Tel = tel;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public String getNaissance() {
		return Naissance;
	}

	public void setNaissance(String naissance) {
		Naissance = naissance;
	}
	
	
}
