<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<html>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
		<script type="text/javascript" src=<c:url value="/resources/app/controller/formCtrl.js"/>></script>
		<body ng-app="myApp">
		<script type="text/javascript" src=<c:url value="/resources/js/submit.js"/>></script>
			<header>
				<div class="col-md-12">
					<%@include file="navbar.jsp" %>
				</div>
			</header>
			<div class="row">
				<div class="col-md-3">
				</div>
				<div class="col-md-6 well well-lg">
					<h1 style="text-align: center">Créer un diplôme</h1>
					<form class="form-horizontal" name="diplome">
					  <div class="form-group">
							<label for="prenom" class="col-xs-12 col-sm-6 col-md-2">Année d'obtention</label>
							<div class="col-sm-10">
								<input type="number" class="form-control" id="annee" placeholder="Année" ng-model="year" name="year" ng-minlength="4" ng-maxlength="4" required>
								<h4 ng-show="!diplome.year.$valid">Année incorrecte</h4>
							</div>
						</div>
					  <div class="form-group">
					    <label for="diplome" class="col-xs-12 col-sm-6 col-md-2">Intitulé du diplome</label>
					    <div class="col-sm-10">
							<input type="text" class="form-control" id="diplome" placeholder="diplome" ng-model="dip" name="dip" required>
							<h4 ng-show="!diplome.dip.$valid">Indiquez un diplôme</h4>
						</div>
					  </div>
					  <div class="form-group">
					    <label for="lieu" class="col-xs-12 col-sm-6 col-md-2">Lieu d'obtention</label>
					    <div class="col-sm-10">
							<input type="text" class="form-control" id="lieu" placeholder="lieu" ng-model="lieu" name="lieu" required>
							<h4 ng-show = "!diplome.lieu.$valid">Indiquez le lieu d'obtention</h4>
						</div>
					  </div>
					  <input ng-disabled="!diplome.$valid" type="submit" value="Ajouter un diplome"></input><span id="ajout"></span>
					</form>
				</div>
				<div class="col-md-3">
				</div>
			</div>
		</body>
	</html>
	