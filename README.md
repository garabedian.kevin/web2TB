# Projet de Web 2.0 du Master GL de St Jérôme de Kevin Garabedian et Julian Hurst
Deux versions du projet existent :
- [CodeIgniter et Smarty](https://gitlab.com/garabedian.kevin/web2TB/tree/master/PhPVersion)
- [JEE (Maven, Spring MVC, JSP, Tomcat, REST)](https://gitlab.com/garabedian.kevin/web2TB/tree/master/JEE)

Le site AngularJS pour le dernier TP :
- [testAngular](https://gitlab.com/garabedian.kevin/web2TB/tree/master/testAngular)
