package util;

public class Filiaire {
	private String nom;
	private int id;
	
	public Filiaire(){
		nom="N/A";
		id=0;
	}
	
	public Filiaire(int id,String nom){
		this.id=id;
		this.nom=nom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
