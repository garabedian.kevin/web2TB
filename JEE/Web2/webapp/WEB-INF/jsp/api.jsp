<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
</head>
<body>
<h1>API</h1>
<p style="color: red;">Attention : il faut que le connector Tomcat soit configuré pour supporter l'UTF-8 pour prendre en compte les accents dans l'URI. Dans server.xml (situé dans le dossier Servers du workspace dans Eclipse) il faut avoir  &lt;Connector URIEncoding=&quot;UTF-8&quot;...</p>
<p>Toutes les données sont données en Json.</p>
<ul>
	<li><a href="etudiants">api/etudiants</a> : Renvoie une liste json de tous les etudiants. Des paramètres peuvent être utilisés pour effectuer une recherche. Par exemple : <a href="etudiants?ville=Marseille">api/etudiants?ville=Marseille</a> renvoie tous les étudiants dont la ville d'origine est Marseille.</li>
	<li><a href="filiaires">api/filiaires</a> : Renvoie une liste json de toutes les filiaires. Des paramètres peuvent être utilisés pour effectuer une recherche. Par exemple : <a href="filiaires?nom=Génie Logiciel">api/filiaires?nom=Génie Logiciel</a> renvoie la filiaire Génie Logiciel.</li>
	<li><a href="diplomes">api/diplomes</a> : Renvoie une liste json de tous les diplômes. Des paramètres peuvent être utilisés pour effectuer une recherche. Par exemple : <a href="diplomes?lieu=Aix-en-Provence">api/diplomes?lieu=Aix-en-Provence</a> renvoie tous les diplômes obtenus à Aix-en-Provence.</li>
	<li>api/etudiants/etu{id} : Une requête GET renvoie l'étudiant référencé par id. Par exemple <a href="etudiants/etu3">api/etudiants/etu3</a> renvoie l'étudiant d'id=3. Une requête DELETE supprimera l'étudiant référencé par id.</li>
	<li>api/filiaires/fil{id} : Une requête GET renvoie l'étudiant référencé par id. Par exemple <a href="filiaires/fil1">api/filiaires/fil1</a> renvoie la filiaire d'id=1. Une requête DELETE supprimera la filiaire référencé par id.</li>
	<li>api/diplomes/dipl{id} : Une requête GET renvoie l'étudiant référencé par id. Par exemple <a href="diplomes/dipl1">api/diplomes/dipl1</a> renvoie le diplôme d'id=1. Une requête DELETE supprimera le diplôme référencé par id.</li>
</ul>
<p>Pour le moment seulement api/etudiants supporte la méthode POST et peut insérer un étudiant en fournissant les données sous format JSON. Par exemple :</p>
<div>
{
<ul class="nobullet">
<li>"prenom":"Foo",</li>
<li>"nom":"Bar",</li>
<li>"filiaire":"Génie Logiciel",</li>
<li>"ville":"Aix-en-Provence",</li>
<li>"tel":"0612345678",</li>
<li>"email":"foo.bar@mail.com",</li>
<li>"annee":"1970-01-01",</li>
<li>"pass":"mdpsupersecret"</li>
</ul>
}
</div>
</body>
</html>