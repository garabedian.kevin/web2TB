(function(){
	var app = angular.module('myApp',[]);

	app.controller('formCtrl', function($scope, $http, $log){

        $scope.getEtu=function(){
            $http({
                method: 'GET',
                url: 'http://localhost:8080/Web2/api/etudiants'
            }).then(function successCallback(response){
                $scope.result = response.data;
                $scope.currentEtu = $scope.result[0];
            }, function errorCallback(response){
                alert('fail');
            });
	    };

        $scope.deleteEtu=function($id){
            $http({
                method: 'DELETE',
                url: 'http://localhost:8080/Web2/api/etudiants/etu'+$id
            }).then(function successCallback(response){
                alert(response.data+" ligne(s) effacée(s)");
                $scope.getEtu();
            }, function errorCallback(response){
                alert('fail');
            });
        };

        $scope.postEtu=function(){
            var data=$scope.fields;
            if($scope.passconfirm==$scope.fields.pass){
                $http({
                    method: 'POST',
                    data: data,
                    url: 'http://localhost:8080/Web2/api/etudiants'
                }).then(function successCallback(response){
                    alert(response.data+" ligne(s) ajoutée(s)");
                    $scope.getEtu();
                }, function errorCallback(response){
                    alert('fail');
                });
            }
            else
                alert("erreur confirmation");
        };

		$scope.getEtu();

	});
})();
