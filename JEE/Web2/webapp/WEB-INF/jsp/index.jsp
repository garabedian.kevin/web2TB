<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	<body>
		<header>
			<div class="col-md-12">
				<%@include file="navbar.jsp" %>
			</div>
		</header>
		<article>							
				<div class="col-md-1">
				</div>
				<div class="col-md-10 table-responsive">
					<table class="table table-striped" id="myTable">
						<thead>
							<tr>
								<th>Prenom</th>
								<th>NOM</th>
								<th>Filiaire</th>
								<th>Age</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="item" items="${list}">
								<tr>
									<td>${item.getPrenom()}</td>
									<td>${item.getNom()}</td>
									<td>${item.getFiliaire()}</td>
									<td>${item.getAge()}</td>
								</tr>
							</c:forEach>							
						</tbody>
					</table>
				</div>
			</article>
	</body>
</html>